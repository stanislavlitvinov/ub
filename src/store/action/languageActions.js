import { LANGUAGE } from '../../utils/constants'

export const changeLanguageReq = payload => ({
  type: LANGUAGE.CHANGE_LANGUAGE_REQ,
  payload
})

export const changeLanguageRes = payload => ({
  type: LANGUAGE.CHANGE_LANGUAGE_RES,
  payload
})