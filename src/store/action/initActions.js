import { INIT } from '../../utils/constants'

export const initPageReq = () => ({
  type: INIT.INIT_PAGE_REQ
})

export const initPageRes = (payload) => ({
  type: INIT.INIT_PAGE_RES,
  payload
})

export const pageIsLoaded = () => ({
  type: INIT.PAGE_LOADED
})