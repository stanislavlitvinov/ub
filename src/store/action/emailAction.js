import { EMAIL } from '../../utils/constants'

export const sendEmailRequest = (payload) => ({
  type: EMAIL.SEND_REQUEST,
  payload
})

export const sendEmailResponse = (payload) => ({
  type: EMAIL.SEND_RESPONSE,
  payload
})

export const toggleIsLoading = () => ({
  type: EMAIL.IS_LOADING
})