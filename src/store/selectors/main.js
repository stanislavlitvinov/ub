import { path } from 'ramda'

export const pageIsLoadedSelector = path(['initPage', 'pageIsLoaded'])