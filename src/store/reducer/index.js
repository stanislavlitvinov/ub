import { emailReducer } from './emailReducer'
import { languageReducer } from './languageReducer'
import { initPageReducer } from './initPageReducer'

export default {
  email: emailReducer,
  i18n: languageReducer,
  initPage: initPageReducer,
}