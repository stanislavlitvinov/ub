import { INIT } from '../../utils/constants'

const initialState = {
  pageIsLoaded: false,

}

export const initPageReducer = (state = initialState, { type, payload }) => {
  switch(type) {
    case INIT.PAGE_LOADED:
      return { ...state, pageIsLoaded: true }
    case INIT.INIT_PAGE_RES:
        return { ...state, pageIsLoaded: true }
    default:
      return { ...state }  
  }
}