import { LANGUAGE, INIT } from '../../utils/constants'

const initialState = {
  loaded: false,
  lang: LANGUAGE.EN,
  items: undefined,
}

export const languageReducer = (state = initialState, { type, payload }) => {
  switch(type) {
    case INIT.INIT_PAGE_RES:
      return { ...state, items: payload }
    case LANGUAGE.CHANGE_LANGUAGE_RES:
      return { ...state, lang: payload.lang, items: payload.items }
    default: 
    return { ...state }
  }
}