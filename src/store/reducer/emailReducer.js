import { EMAIL } from '../../utils/constants'

const initialState = {
  isLoading: false,
}

export const emailReducer = (state = initialState, { type, payload }) => {
  switch(type) {
    case EMAIL.IS_LOADING: 
      return { ...state, isLoading: !state.isLoading }
    default:
      return state
  }
}