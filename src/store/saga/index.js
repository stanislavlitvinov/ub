import { all, fork } from 'redux-saga/effects'
import initSaga from './initSaga'
import emailSaga from './emailSaga'
import languageSaga from './languageSaga'

export default function* root() {
  yield all([
    fork(initSaga),
    fork(emailSaga),
    fork(languageSaga),
  ])
}