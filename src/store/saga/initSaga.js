import { all, call, put, takeLatest, select } from 'redux-saga/effects'

import { INIT } from '../../utils/constants'
import { initPageRes, pageIsLoaded } from '../action/initActions'
import dictionaryEN from '../i18n/language_EN'
import dictionaryES from '../i18n/language_ES'


function* initPageSaga() {
  console.log('initPage')
  
  yield put(initPageRes(dictionaryEN))
  yield put(pageIsLoaded())
}

export default function* root() {
  yield all([
    takeLatest(INIT.INIT_PAGE_REQ, initPageSaga)
  ])
}