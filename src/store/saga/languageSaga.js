import { all, call, put, takeLatest } from 'redux-saga/effects'

import { LANGUAGE } from '../../utils/constants'
import { changeLanguageRes, changeLanguageReq } from '../action/languageActions'
import EnglishDictionary from '../i18n/language_EN'
import SpanishDictionary from '../i18n/language_ES'





function* changeLanguageSaga({ payload }) {
  if (payload === LANGUAGE.ES) {
    yield put(changeLanguageRes({lang: payload, items: SpanishDictionary}))
    return
  }
  if (payload === LANGUAGE.EN) {
    yield put(changeLanguageRes({lang: payload, items: EnglishDictionary}))
    return
  }
  
}

export default function* root() {
  yield all([
    takeLatest(LANGUAGE.CHANGE_LANGUAGE_REQ, changeLanguageSaga)
  ])
}