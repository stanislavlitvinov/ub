import { all, call, put, takeLatest } from 'redux-saga/effects'

import { EMAIL } from '../../utils/constants'


function* emailSendRequestSaga(payload) {
  console.log('email send request')
}

export default function* root() {
  yield all([
    takeLatest(EMAIL.SEND_REQUEST, emailSendRequestSaga)
  ])
}