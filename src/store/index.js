import { applyMiddleware, createStore, compose, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducer'
import rootSaga from './saga'
import { initI18N } from './i18n/index'

const reducer = combineReducers(rootReducer)

const sagaMiddleware = createSagaMiddleware()

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(sagaMiddleware)))

sagaMiddleware.run(rootSaga)

initI18N(store)