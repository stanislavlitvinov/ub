import React, { useEffect } from 'react';
import './App.scss';
import { connect } from 'react-redux'
import 'antd/dist/antd.css'
import { initPageReq } from './store/action/initActions'
import Language from './components/Language'
import { pageIsLoadedSelector } from './store/selectors/main'
import { Header } from './components/Header'
import { SubmitForm } from './components/SubmitForm'



const App = (props) => {
  const { initPage, pageIsLoaded, initPageReq } = props
  useEffect(() => {
    initPageReq()
  }, [])

  return (
    <div >
      <Header></Header>
        {pageIsLoaded && <Language />}
      <SubmitForm />  
    </div>
  );
}

const mapStateToProps = state => ({
  pageIsLoaded: pageIsLoadedSelector(state),
})

const mapDispatchToProps = ({
  initPageReq,
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
