import React from 'react'
import { Row, Col } from 'antd'
import { MenuHeader } from './MenuHeader'

export const Header = props => {

  return (
    <header>
      Header
      <MenuHeader />
    </header>
  )
}
