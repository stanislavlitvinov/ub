import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Button, Select } from 'antd'
import { changeLanguageReq } from '../store/action/languageActions'
import { LANGUAGE } from '../utils/constants'
import { i18nLangSelector } from '../store/selectors/i18n-selector'
import { translate } from '../store/i18n/index'

const { EN, ES } = LANGUAGE
const { Option } = Select

const languages = [{
    langType: 'English',
    value: EN
  }, 
  {
    langType: 'Espaniol',
    value: ES
},] 

const Language = props => {

  const { changeLanguageReq } = props
  useEffect(() => {
    console.log('props', props)
  }, [])
  const handleChange = (value) => {
    changeLanguageReq(value)
  }

  return (
    <div className="language-container">
            {translate('test')}

      <Select defaultValue={languages[0].langType} onChange={value => handleChange(value)}>
        {languages.map((lang, i) => (
          <Option key={i} value={lang.value}>
            {lang.langType}
          </Option>
        ))}
      </Select>
    </div>
  )

}

const mapStateToProps = state => ({
  lang: i18nLangSelector(state)
})

const mapDispatchToProps = ({
  changeLanguageReq
})

export default connect(mapStateToProps, mapDispatchToProps)(Language)