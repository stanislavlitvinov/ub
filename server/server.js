const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path');
const { createProxyMiddleware } = require('http-proxy-middleware')



const PORT = 8080

const app = express();

app.use(cors())

app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(bodyParser.json())

app.use(
  '/api',
  createProxyMiddleware({
    target: 'http://localhost:3000',
    changeOrigin: true,
  })
)


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "YOUR-DOMAIN.TLD"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



app.use(express.static(path.join(__dirname, '../build')));

app.get('/',  (req, res) => {

  res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

app.listen(PORT, () => {
  console.log(`server is running on ${PORT} port`)
});